/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.EasyParkingmodelo;
import com.example.EasyParkingmodelo.usuario;
//import para conexion de Usuario -- vehiculo  ### comentario de cesar
//idUsuario
import com.example.EasyParkingmodelo.reportes;
//para conexion entre Vehiculos --- Reportes  ###comentario de cesar
//idReporte()
import com.example.EasyParkingmodelo.ticket;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
//para conexion entre Vehiculos --- Ticket  ### comentario de cesar
// codFactura()
/**
 *
 * @author Administración
 */
@Table( name="vehiculos")
@Entity
public class vehiculos implements Serializable {
     transient JdbcTemplate jdbcTemplate;
    @Autowired
   
         @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
     @ManyToOne 
   @JoinColumn(name="idUsuario")
   private usuario idUsuario;
    @Column(name="Placa")
    private String placa;
    @Column(name="modelo")
    private int modelo;
    @Column(name="Tipo_Tarifa")
    private int codTarifa;
    @Column(name="TipoVehiculo")
    private String tipoVehiculo;
    @Column(name="Fecha")
    private String fecha;
     @Column(name="Marca")
    private String marca;

   

    public vehiculos() {
    }
    
     
    //constructor

    public vehiculos( String placa, int modelo, int codTarifa, String tipoVehiculo, String fecha, String marca) {
        super();
        this.placa = placa;
        this.modelo = modelo;
        this.codTarifa = codTarifa;
        this.tipoVehiculo = tipoVehiculo;
        this.fecha = fecha;
        this.marca=marca;
      
    }

  
  

//metodos


    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public int getCedula() {
        return modelo;
    }

    public void setCedula(int cedula) {
        this.modelo = cedula;
    }

    public int getCodTarifa() {
        return codTarifa;
    }

    public void setCodTarifa(int codTarifa) {
        this.codTarifa = codTarifa;
    }

    public String getTipoVehiculo() {
        return tipoVehiculo;
    }

    public void setTipoVehiculo(String tipoVehiculo) {
        this.tipoVehiculo = tipoVehiculo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getModelo() {
        return modelo;
    }

    public void setModelo(int modelo) {
        this.modelo = modelo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(usuario idUsuario) {
        this.idUsuario = idUsuario;
    }
    

    @Override
    public String toString() {
        return "Vehiculos{" + "idUsuario=" + idUsuario + ", placa=" + placa + ", modelo=" + modelo + ", codTarifa=" + codTarifa + ", tipoVehiculo=" + tipoVehiculo + ", fecha=" + fecha + ", marca=" + marca + '}';
    }



  

    
    public String guardar()throws ClassNotFoundException, SQLException{
         String sql="INSERT INTO vehiculos (idUsuario, Placa, modelo, Tipo_Tarifa,TipoVehiculo,Fecha, Marca  ) VALUES(?,?,?,?,?,?,?) ";   
    return sql;
    }
    
    public boolean consultar() throws ClassNotFoundException, SQLException{
        String sql= "SELECT idUsuario,Placa, modelo, Tipo_Tarifa,TipoVehiculo,Fecha, Marca FROM registros WHERE idUsuario = ?";
         List<vehiculos> registros = jdbcTemplate.query(sql, (rs, rowNom)
                 -> new vehiculos(
                         
                         rs.getString("Placa"),
                         rs.getInt("modelo"),
                         rs.getInt("Tipo_Tarifa"),
                         rs.getString("TipoVehiculo"),
                         rs.getString("Fecha"),
                         rs.getString("Marca")
                         
                        
                         
                                         ), new Object []{this.getIdUsuario()});
          if (registros != null && !registros.isEmpty()){
            this.setIdUsuario(registros.get(0).getIdUsuario());
            this.setPlaca(registros.get(0).getPlaca());
            this.setModelo(registros.get(0).getModelo());
            this.setCodTarifa(registros.get(0).getCodTarifa());
            this.setTipoVehiculo(registros.get(0).getTipoVehiculo());
            this.setFecha(registros.get(0).getFecha());
            this.setMarca(registros.get(0).getMarca());
           
            
            
            return true;         
       }
 
        else {
            return false;
        }
         
    }
    
    public List<vehiculos>consultarTodo(int idUsuario) throws ClassNotFoundException, SQLException{
        String sql= "SELECT idUsuario,Placa, modelo, Tipo_Tarifa,TipoVehiculo,Fecha, Marca FROM registros WHERE idUsuario = ?";
         List<vehiculos> registros = jdbcTemplate.query(sql, (rs, rowNom)
                 -> new vehiculos(
                         
                         rs.getString("Placa"),
                         rs.getInt("modelo"),
                         rs.getInt("idTarifa"),
                         rs.getString("TipoVehiculo"),
                         rs.getString("Fecha"),
                         rs.getString("Marca")
                        
                         
                                         ), new Object []{this.getIdUsuario()});
    return registros;
    }
    
    
    
    public String actualizar ()throws ClassNotFoundException, SQLException{
        String sql= "UPDATE vehiculos SET placa=?,  modelo=?, Tipo_Tarifa=?,TipoVehiculo=?,Fecha=?, Marca=?  where idUsuario";
        return sql;
    }
      public boolean borrar ()throws ClassNotFoundException, SQLException{
        String sql ="DELETE FROM Vehiculos where idUsuario=?";
        Connection c= jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setString(1, this.getPlaca());
        ps.execute();
        return true;
        
    }
}
