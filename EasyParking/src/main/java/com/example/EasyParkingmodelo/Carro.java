/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.EasyParkingmodelo;

import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author INTEL
 */
public class Carro extends vehiculos{

    @Override
    public boolean borrar() throws ClassNotFoundException, SQLException {
        this.setTipoVehiculo("carro");
        return super.borrar(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    @Override
    public String actualizar() throws ClassNotFoundException, SQLException {
        this.setTipoVehiculo("carro");
        return super.actualizar(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    @Override
    public List<vehiculos> consultarTodo(int idUsuario) throws ClassNotFoundException, SQLException {
        this.setTipoVehiculo("carro");
        return super.consultarTodo(idUsuario); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    @Override
    public boolean consultar() throws ClassNotFoundException, SQLException {
        this.setTipoVehiculo("carro");
        return super.consultar(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    @Override
    public String guardar() throws ClassNotFoundException, SQLException {
        this.setTipoVehiculo("carro");
        return super.guardar(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }
    
    
    
}
