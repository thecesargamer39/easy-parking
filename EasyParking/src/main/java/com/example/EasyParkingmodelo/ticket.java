/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.EasyParkingmodelo;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author GRUPO O5 - 02
 */
@Entity
@Table (name="ticket")
public class ticket implements Serializable {
    
    @Autowired
   transient JdbcTemplate jdbcTemplate;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name="idTicket")
     private int IdTicket;
   @Column(name="Hora_Entrada")   
    private String horaEntrada;
   @Column(name="Hora_Salida")
    private String horaSalida;
   @Column(name="Valor")
    private int valor;
   @Column(name="LugarDisponible")
    private int lugar;
  
   
   @ManyToOne 
   @JoinColumn(name="idUsuario")
   private usuario idUsuario;
   
    public ticket() {
    }
    
//constructor
    public ticket( String horaEntrada, String horaSalida, int valor, int IdRecibo, int lugar) {
        super();
       
        this.horaEntrada = horaEntrada;
        this.horaSalida = horaSalida;
        this.valor = valor;
        this.IdTicket = IdRecibo;
        this.lugar= lugar;
       
        
        
    }
  
        
//metodos



 

    public String getHoraEntrada() {
        return horaEntrada;
    }

    public void setHoraEntrada(String horaEntrada) {
        this.horaEntrada = horaEntrada;
    }

    public String getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(String horaSalida) {
        this.horaSalida = horaSalida;
    }

    public int getValor() {
        
        
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public int getIdTicket() {
        return IdTicket;
    }

    public void setIdTicket(int IdTicket) {
        this.IdTicket = IdTicket;
    }

    public int getLugar() {
        return lugar;
    }

    public void setLugar(int lugar) {
        this.lugar = lugar;
    }

    public usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(usuario idUsuario) {
        this.idUsuario = idUsuario;
    }


    @Override
    public String toString() {
        return "Ticket{" + "IdTicket=" + IdTicket + ", horaEntrada=" + horaEntrada + ", horaSalida=" + horaSalida + ", valor=" + valor + ", lugar=" + lugar + ", idUsuario=" + idUsuario + '}';
    }
    

  
    

   
    

  // CRUD - R - Read
    public boolean consultar() throws ClassNotFoundException, SQLException{
      String sql = "SELECT idTicket,Hora_Entrada, Hora_salida, ValorTarifa,idUsuario, LugarDisponible FROM IdUsuario";
        List<ticket> tickets = jdbcTemplate.query(sql, (rs, rowNum)
                -> new ticket(
                       
                        rs.getString("Hora_salida"),
                        rs.getString("Hora_Entrada"),
                        rs.getInt("LugarDisponible"),
                         rs.getInt("idTicket"),
                        rs.getInt("Valor")
                         
                        
                        
                        
                ), new Object[]{this.getIdUsuario()});

        if (tickets != null && !tickets.isEmpty()) {
            
            this.setHoraEntrada(tickets.get(0).getHoraEntrada());
            this.setHoraSalida(tickets.get(0).getHoraSalida());
            this.setIdTicket(tickets.get(0).getIdTicket());
            this.setValor(tickets.get(0).getValor());
            this.setLugar(tickets.get(0).getLugar());
          
            

            return true;

        } else {
            return false;
        }   
        
    }
    
    //CRUD - C - Create
    public String guardar()throws ClassNotFoundException, SQLException{
         String sql = "INSERT INTO ticket (idTicket, Hora_Entrada,Hora_Salida , LugarDisponible,ValorTarifa) VALUES (?,?,?,?,?)";
        return sql;
    }
    
 public String actualizar ()throws ClassNotFoundException, SQLException{
        String sql= "UPDATE ticket SET Hora_Entrada=?,  Hora_Salida=?, LugarDisponible=?,ValorTarifa=?  where idUsuario";
        return sql;
    }
 
  public boolean borrar ()throws ClassNotFoundException, SQLException{
        String sql ="DELETE FROM ticket where idUsuario=?";
        Connection c= jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setInt(1, this.getIdTicket());
        ps.execute();
        return true;
        
    }
    
}
