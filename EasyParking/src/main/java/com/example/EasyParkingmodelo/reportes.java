/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.EasyParkingmodelo;

import java.sql.SQLException;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author Administración
 */
@Table (name="reportes")
public class reportes extends vehiculos{
    //Atributos
        @Autowired
     transient JdbcTemplate jdbcTemplate;
         @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="idReporte")

    private int idReporte;
    
     @Column(name="Tipo_Reporte")
    private String tipoReporte;
    
    
    //Constructor

    public reportes(int idReporte, String tipoReporte, String placa, int modelo, int codTarifa, String tipoVehiculo, String fecha, String marca) {
        super(placa, modelo, codTarifa, tipoVehiculo, fecha, marca);
        this.idReporte = idReporte;
        this.tipoReporte = tipoReporte;
    }

  
   

    public int getIdReporte() {
        return idReporte;
    }

    public void setIdReporte(int idReporte) {
        this.idReporte = idReporte;
    }

    public String getTipoReporte() {
        return tipoReporte;
    }

    public void setTipoReporte(String tipoReporte) {
        this.tipoReporte = tipoReporte;
    }

    @Override
    public String toString() {
        return super.toString() ; // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    @Override
    public boolean consultar() throws ClassNotFoundException, SQLException {
        return super.consultar(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    @Override
    public List<vehiculos> consultarTodo(int idUsuario) throws ClassNotFoundException, SQLException {
        return super.consultarTodo(idUsuario); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }
    
    


    
}
