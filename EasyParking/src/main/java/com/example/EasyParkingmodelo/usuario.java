/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.EasyParkingmodelo;

import com.example.EasyParkingmodelo.cliente;
import java.io.Serializable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
// import para conexion entre tabla Cliente y tabla Usuario  ###comentario de Cesar
// idCliente()

/**
 *
 * @author GRUPO O5 - 02
 */
@Entity
@Table(name = "usuario")

public class usuario implements Serializable {
    
//    @OneToOne
//    @JoinColumn(name="idCliente")
//    private Cliente idCliente;

//PAQUETE QUE EJECUTA LAS CONSULTAS
    @Autowired
    transient JdbcTemplate jdbcTemplate; //Palabra clave en Java para serealizar

//atributos
    
//    @OneToMany
//    @JoinTable(name = "idUsuario")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "idUsuario")
    private int idUsuario;

    @Column(name = "cedula")
    private int cedula;

    @Column(name = "nombres")
    private String nombres;

    @Column(name = "apellidos")
    private String apellidos;

    @Column(name = "telefono")
    private int telefono;

    @Column(name = "direccion")
    private String direccion;
     @Column(name = "TipoUsuario")
    private boolean tipoUser;

    //constructor
    public usuario(int idUsuario, int cedula, String nombres, String apellidos, int telefono, String direccion,boolean tipoUser) {
        super();//Hereda de la clase principal
        this.idUsuario = idUsuario;
        this.cedula = cedula;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.telefono = telefono;
        this.direccion = direccion;
        this.tipoUser=tipoUser;
    }

    public usuario() {
    }
//metodos

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public boolean isTipoUser() {
        return tipoUser;
    }

    public void setTipoUser(boolean tipoUser) {
        this.tipoUser = tipoUser;
    }

    @Override
    public String toString() {
        return "Usuario{" + "idUsuario=" + idUsuario + ", cedula=" + cedula + ", nombres=" + nombres + ", apellidos=" + apellidos + ", telefono=" + telefono + ", direccion=" + direccion + ", tipoUser=" + tipoUser + '}';
    }
    
    


    //CRUD - C - create
    public String guardar() throws ClassNotFoundException, SQLException {
        String sql = "INSERT INTO usuario(idUsuario, cedula, nombres, apellidos, telefono, direccion,TipoUsuario) VALUES (?,?,?,?,?,?)";
        return sql;
    }

    //CRUD - R - Read
    public boolean consultar() throws ClassNotFoundException, SQLException {
        String sql = "SELECT idUsuario, Cedula, Nombres, Apellidos, Telefono,TipoUsuario,Direccion FROM idUsuario";
        List<usuario> usuario = jdbcTemplate.query(sql, (rs, rowNum)
                -> new usuario(
                        rs.getInt("idUsuario"),
                        rs.getInt("Cedula"),
                        rs.getString("Nombres"),
                        rs.getString("Apellidos"),
                        rs.getInt("Telefono"),
                        rs.getString("Direccion"),
                        rs.getBoolean("TipoUsuario")
                        
                ), new Object[]{this.getIdUsuario()});

        if (usuario != null && usuario.size() > 0) {
            this.setIdUsuario(usuario.get(0).getIdUsuario());
            this.setCedula(usuario.get(0).getCedula());
            this.setNombres(usuario.get(0).getNombres());
            this.setApellidos(usuario.get(0).getApellidos());
            this.setTelefono(usuario.get(0).getTelefono());
            this.setDireccion(usuario.get(0).getDireccion());
            this.setTipoUser(usuario.get(0).isTipoUser());

            return true;

        } else {
            return false;
        }
        //....
    }
        
    // CRUD - U - UPDATE
    public String actualizar()throws ClassNotFoundException, SQLException{
        String sql = "UPDATE usuario SET cedula=?, nombres =?, apellidos=? telefono=?, direccion=? WHERE idUsuario=?";
        return sql;
    }

    // CRUD - D - DELETE
    public boolean borrar() throws ClassNotFoundException, SQLException {
        String sql = "DELETE FROM usuario where idUsuario=?";
        Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setInt(1, this.getIdUsuario());
        ps.execute();
        ps.close();
        return true;
    }
    
    
    
    
}
