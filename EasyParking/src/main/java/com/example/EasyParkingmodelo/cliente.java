/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.EasyParkingmodelo;

import java.sql.SQLException;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.springframework.beans.factory.annotation.Autowired;




/**
 *
 * @author GRUPO O5 - 02
 */
@Table (name="cliente")
public class cliente extends usuario {
       @Autowired
   
         @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="idCliente")
  
    //Atributos
    private int idCliente;
    
    
    //Constructor
   
    
    //Metodos


    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    @Override
    public String toString() {
        return super.toString(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

   
    
    @Override
    public String actualizar() throws ClassNotFoundException, SQLException {
       
        this.setTipoUser(true); // si true es cliente 
        
        return super.actualizar(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    @Override
    public boolean consultar() throws ClassNotFoundException, SQLException {
        this.setTipoUser(true);
        return super.consultar(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }
    
    
 
    
    
}
